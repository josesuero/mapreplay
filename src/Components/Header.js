import React, { Component } from 'react'

import firebase from 'firebase';
import { Modal, Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Cookies from 'universal-cookie';

import { Context } from './AuthContext';
import DB from './db';
import Login from './Login';

/**
 * Header component, displayed at the top of all pages
 * holds instruction and login modals
 * Holds signout logic
 */
class Header extends Component {

  constructor() {
    super();
    this.state = {
      showLoginModal: false,
      showInstructionModal: false
    }

    this.db = new DB();
    this.signOut = this.signOut.bind(this);
    this.showLoginModal = this.showLoginModal.bind(this);
    this.showInstructionModal = this.showInstructionModal.bind(this)
  }

  /**
   * if this is the first time loading, load instructions
   */
  componentWillMount() {
    this.setState({ showInstructionModal: this.props.first });
  }

  /**
   * SignOut, generate a new cookie and reload page.
   */
  signOut() {
    firebase.auth().signOut();
    const cookies = new Cookies();
    try {
      cookies.set('uid', this.db.generatePushID(), { path: '/' });
      window.location.href = '/'
    } catch(e){
      console.error(e);
    }
  }

  /**
   * Toggle instructions modal
   * @param {*} e 
   */
  showInstructionModal(e) {
    this.setState({ showInstructionModal: !this.state.showInstructionModal });
  }

  /**
   * Toggle Login Modal
   * @param {*} e 
   */
  showLoginModal(e) {
    if (e) {
      e.preventDefault();
    }
    this.setState({ showLoginModal: !this.state.showLoginModal });
  }

  render() {
    return (
      <div>
        <Modal show={this.state.showLoginModal} onHide={this.showLoginModal}>
          <Modal.Header closeButton>
            <Modal.Title>Login</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <Login onLogin={this.showLoginModal} visible={this.state.showLoginModal} />
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.showLoginModal}>Close</Button>
          </Modal.Footer>
        </Modal>

        <Modal show={this.state.showInstructionModal} onHide={this.showInstructionModal}>
          <Modal.Header closeButton>
            <Modal.Title>Welcome and thanks for checking out this site!</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <p>I hope you find it useful to be able to check locations.</p>
            <p>I am providing this tool as-is, since I have personally found it useful, but I can not make any guarantees about its accuracy.</p>

            <p>I did my best to crop out the non-highlighted regions of the map, but please double check your work independently.</p>
            <p>Information:</p>
            <ul>
              <li>Scroll up and down to zoom in and out</li>
              <li>Click and drag to pan around</li>
              <li>Click and release on any location to drop a pin</li>
              <li>Right click on a pin to remove it</li>
              <li>As long as the site exists, it will always be free to use as a guest</li>
              <li>If you wish to save your pins, just enter your name and a password to create an account</li>
              <li>Once logged in, your changes will be saved automatically</li>
              <li>Note: If this tool becomes popular and I start to add a lot more features, I may later decide to charge money for creating a new account and saving pins</li>
              <li>So if you want to lock in lifetime access for free, you may want to create an account soon</li>
            </ul>
            <p>Best of luck</p>
          </Modal.Body>
          <Modal.Footer>
            <Button onClick={this.showInstructionModal}>Continue</Button>
          </Modal.Footer>
        </Modal>

        <div className="App-header">
          <span>
            <a className='header' href="#instructions" onClick={this.showInstructionModal}>Instructions</a>
          </span>
          <span style={{ position: "absolute", right: 2, top: 2 }}>
            <Context.Consumer>
              {value => {
                if (value.user) {
                  return (
                    <span>
                      <span>{value.user.displayName} (<Link to={'/'} className='header' style={{ fontSize: 10 }} onClick={this.signOut}>Sign-out</Link>)</span><br />
                      <span>
                        {value.user.isAdmin ? <Link className='header' to={'/admin'}>Admin</Link> : <span />}
                      </span>
                    </span>
                  )
                } else {
                  return <span>You are currently browsing as Guest.<br /> <a className='header' href="#login" onClick={this.showLoginModal}>Login</a> to save your map</span>
                }
              }
              }
              {/* [ Save map ] [ Log in ] */}
            </Context.Consumer>
          </span>
        </div>
        <div className="">
          <header>
          </header>
        </div>
      </div >
    )
  }
}

export default Header

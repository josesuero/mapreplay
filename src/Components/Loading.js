import React from 'react';
import { RingLoader } from 'react-spinners';

/**
 * Loading component, presents an animation while user waits
 */
export default (props) => (<div className='loading'>
    <RingLoader
        color={'#123abc'}
        loading={props.loading}
        size={150}
    />
</div>)
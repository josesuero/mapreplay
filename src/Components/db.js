import firebase from 'firebase';
import axios from 'axios';

/**
 * Firebase interface
 */
class DB {
    /**
     * db firebase client
     */
    constructor() {
        this.db = firebase.firestore();
        //firebase.firestore.setLogLevel('debug');
    }

    /**
     * Adds an user position to the DB
     * @param {*} user user
     * @param {*} pos  position Object
     */
    add(user, pos) {
        this.db.collection("positions").add({
            user,
            ...pos,
            date: new Date()
        })
            .then(function (docRef) {
                //console.log("Document written with ID: ", docRef.id);
            })
            .catch(function (error) {
                console.error("Error adding document: ", error);
            });
    }

    /**
     * Adds marker to the db
     * @param {*} marker 
     */
    addMarker(marker) {
        return this.db.collection("markers").doc(marker.key).set(marker);
    }

    /**
     * deletes all data for positions
     */
    deleteData() {
        const $this = this;
        return this.db.collection('positions').get()
            .then(function (querySnapshot) {
                var batch = $this.db.batch();

                querySnapshot.forEach(function (doc) {
                    batch.delete(doc.ref);
                });

                return batch.commit();
            });
    }

    /**
     * Gets user record by user param
     * @param {*} user 
     */
    get(user) {
        return this.db.collection("positions")
            .where("user", "==", user)
            .orderBy("date")
            .get();
    }

    /**
     * Gets all markers for user
     * @param {*} user 
     */
    getMarkers(user) {
        return this.db.collection("markers")
            .where("user", "==", user)
            .get();

    }

    /**
     * Gets user record by firebase auth uid
     * @param {*} uid 
     */
    getUserByRegUid(uid) {
        return this.db.collection('users').where("uid", "==", uid).get()
    }

    /**
     * gets user by cookie id
     * @param {*} cookie 
     */
    getUser(cookie) {
        return this.db.collection('users').doc(cookie).get()
    }

    /**
     * Gets all users on DB
     */
    getUsers() {
        return this.db.collection('users')
            .get();

    }

    /**
     * Generate an unique ID 
     */
    generatePushID() {
        // Modeled after base64 web-safe chars, but ordered by ASCII.
        var PUSH_CHARS = '-0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ_abcdefghijklmnopqrstuvwxyz';

        // Timestamp of last push, used to prevent local collisions if you push twice in one ms.
        var lastPushTime = 0;

        // We generate 72-bits of randomness which get turned into 12 characters and appended to the
        // timestamp to prevent collisions with other clients.  We store the last characters we
        // generated because in the event of a collision, we'll use those same characters except
        // "incremented" by one.
        var lastRandChars = [];

        return (function () {
            var now = new Date().getTime();
            var duplicateTime = (now === lastPushTime);
            lastPushTime = now;

            var timeStampChars = new Array(8);
            for (var i = 7; i >= 0; i--) {
                timeStampChars[i] = PUSH_CHARS.charAt(now % 64);
                // NOTE: Can't use << here because javascript will convert to int and lose the upper bits.
                now = Math.floor(now / 64);
            }
            if (now !== 0) throw new Error('We should have converted the entire timestamp.');

            var id = timeStampChars.join('');

            if (!duplicateTime) {
                for (i = 0; i < 12; i++) {
                    lastRandChars[i] = Math.floor(Math.random() * 64);
                }
            } else {
                // If the timestamp hasn't changed since last push, use the same random number, except incremented by 1.
                for (i = 11; i >= 0 && lastRandChars[i] === 63; i--) {
                    lastRandChars[i] = 0;
                }
                lastRandChars[i]++;
            }
            for (i = 0; i < 12; i++) {
                id += PUSH_CHARS.charAt(lastRandChars[i]);
            }
            if (id.length !== 20) throw new Error('Length should be 20.');

            return id;
        })();
    };

    /**
     * Removes marker from DB
     * @param {*} key 
     */
    removeMarker(key) {
        this.db.collection("markers").doc(key).delete().then(function () {
            //console.log("Document successfully deleted!");
        }).catch(function (error) {
            console.error("Error removing document: ", error);
        });
    }

    /**
     * Remove positions from the DB
     * @param {*} uid 
     */
    removePositions(uid) {
        const $this = this;
        return this.db.collection('positions').where("user", "==", uid).get()
            .then(function (querySnapshot) {
                let batch = $this.db.batch();
                console.log(querySnapshot.size);

                let count = 0;
                querySnapshot.forEach(function (doc) {
                    batch.delete(doc.ref);
                    if (count % 500 === 0) {
                        batch.commit();
                        batch = $this.db.batch();
                    }
                });
                return batch.commit();
            });
    }

    /**
     * Removes user from the DB
     * @param {*} uid 
     */
    removeUser(uid) {
        return this.db.collection('users').doc(uid).delete();
    }

    /**
     * Updates a marker label
     * @param {*} key 
     * @param {*} labelText 
     */
    updateMarker(key, labelText) {
        //
        const documentRef = this.db.collection('markers').doc(key);
        return documentRef.update({ labelText });
}

    /**
     * Updates or creates user record
     * @param {*} user 
     */
    updateUser(user) {
        //
        const documentRef = this.db.collection('users').doc(user.cookie);
        return axios.get('https://ipinfo.io?token=7a8be00b08b6b3').then(ip => {
            const userObj = { ...user, ...ip.data}
            return documentRef.get().then(documentSnapshot => {
                if (documentSnapshot.exists) {
                    return documentRef.update(userObj);
                } else {
                    return documentRef.set({...userObj, createdDate: new Date()});
                }
                
            });
        });
    }
}

export default DB;
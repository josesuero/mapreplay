import React from "react"

import { Modal, Button } from 'react-bootstrap';
import { withScriptjs, withGoogleMap, GoogleMap, KmlLayer, Marker, } from "react-google-maps";
//import MarkerWithLabel from "react-google-maps/lib/components/addons/MarkerWithLabel";
import { compose, withProps } from "recompose";

import { mapConfig } from '../config';
import DB from './db';
import Loading from './Loading';

/**
 * Google map key
 */
const key = mapConfig.key;
/**
 * Global variables
 * db: Firestore client
 * map: google map object
 * user: current user
 * _markers: current user markers
 * play: play mode
 * lastpos: last position stored
 */
let db, map, user, _markers = {}, play = true, lastpos = {};

/**
 * Map Component, all map logic to display and store positions
 */

 /**
  * If not on play mode Saves current position, zoom and markers when an event is triggered
  */
function onPositionChanged() {
    if (!play) {
        const center = map.getCenter();
        //make new markers object
        const newMarkers = JSON.parse(JSON.stringify(_markers));
        const pos = { lat: center.lat(), lng: center.lng(), zoom: map.getZoom(), markers: newMarkers };
        if (JSON.stringify(pos) !== JSON.stringify(lastpos)) {
            lastpos = pos;
            db.add(user, pos);
        }
    }
}

/**
 * Generates a marker object to be displayed
 * if marker has a label generates a label object
 * @param {*} key 
 * @param {*} onMarkerClick 
 */
function generateMarker(key, onMarkerClick) {
    const value = _markers[key];
    let label = '';
    if (value.labelText && value.labelText !== '') {
        label = {
            text: value.labelText,
            color: mapConfig.labelColor,
            fontSize: mapConfig.labelSize,
        }
    }

    return (<Marker
        key={key}
        label={label}
        {...value}
        onClick={() => onMarkerClick(key)}
    />
    )
}

/**
 * Main map component, loads google map, KML and all markers
 * triggers events for zoom, dragend to record positions, 
 * click creates a new marker
 */
const Map = compose(
    withProps({
        googleMapURL: `https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=geometry,drawing,places&key=${key}`,
        loadingElement: <div style={{ height: `100%` }} />,
        containerElement: <div style={{ height: `85vh` }} />,
        mapElement: <div style={{ height: `100%` }} />,
    }),
    withScriptjs,
    withGoogleMap
)((props) => {
    play = props.play;
    user = props.user;
    _markers = props.markers;

    return (<div style={{ height: 100 }}>
        <GoogleMap
            defaultZoom={props.pos.zoom}
            zoom={props.pos.zoom}
            defaultCenter={{ lat: props.pos.lat, lng: props.pos.lng }}
            center={{ lat: props.pos.lat, lng: props.pos.lng }}
            //onBoundsChanged={onPositionChanged}
            onZoomChanged={onPositionChanged}
            onDragEnd={onPositionChanged}
            onClick={props.onClick}
            mapTypeId={'satellite'}
            gestureHandling={'greedy'}
            onIdle={props.onIdle}
            ref={input => map = input}
        >
            {props.markers && Object.keys(props.markers).map(key => generateMarker(key, props.onMarkerClick))}
            <KmlLayer
                url={mapConfig.kmz}
                options={{
                    preserveViewport: true
                    , clickable: false
                    , suppressInfoWindows: true
                }}
                onClick={props.onClick}
            />
        </GoogleMap>
    </div>);
}
);

/**
 * Map wrapper
 * Load map component
 */
class MapComponent extends React.Component {
    constructor() {
        super();
        db = new DB();

        this.handleClose = this.handleClose.bind(this);
        this.removeMarker = this.removeMarker.bind(this);
        this.updateMarker = this.updateMarker.bind(this);

        this.state = {
            currentMarker: null,
            currentLabel: '',
            isMarkerShown: false,
            loading: true,
            show: false
        }
    }

    /**
     * loads all markers
     */
    componentWillMount() {
        this.setState({
            markers: this.props.markers || {}
        });
    }

    /**
     * Update map on every change
     * @param {*} nextProps 
     * @param {*} nextState 
     */
    shouldComponentUpdate(nextProps, nextState) {
        return true;
    }

    /**
     * Adds marker to state and DB, triggers position change
     * @param {*} pos 
     * @param {*} test 
     */
    addMarker(pos, test) {
        if (this.props.play) {
            return;
        }
        const markers = this.state.markers;
        const key = db.generatePushID();
        markers[key] = {
            key,
            user: this.props.user,
            date: new Date(),
            position: { lat: pos.latLng.lat(), lng: pos.latLng.lng() }
        };
        db.addMarker(markers[key]).then(() => {
            console.log('marker added');
        });
        _markers = markers;
        this.setState({ markers });
        onPositionChanged();
        return true;
    }

    /**
     * Map has finish loading event
     */
    finishLoading() {
        this.setState({ loading: false });
    }

    /**
     * Marker modal has closed
     */
    handleClose() {
        this.setState({ currentMarker: null, currentLabel: '', show: false });
        //onPositionChanged();
    }

    /**
     * marker click event
     * show marker modal
     */
    handleMarkerClick = (key) => {
        this.setState({ currentMarker: key, currentLabel: _markers[key].labelText ? _markers[key].labelText : '', show: true });
    }

    /**
     * Loading component
     */
    loadingDiv() {
        if (this.props.loading || this.state.loading) {
            return <Loading />
        }
        return <span />
    }

    /**
     * Remove marker
     */
    removeMarker() {
        //console.log(marker);
        const key = this.state.currentMarker
        const markers = this.state.markers;
        delete markers[key];
        db.removeMarker(key);
        this.setState({ markers });
        this.handleClose();
    }

    /**
     * Update marker label
     */
    updateMarker() {
        const { currentLabel, currentMarker } = this.state;
        _markers[currentMarker].labelText = currentLabel;
        db.updateMarker(currentMarker, currentLabel);
        this.handleClose();
    }

    render() {
        const markers = play ? this.props.markers : this.state.markers;
        let pos = this.props.pos;
        if (lastpos.lat) {
            pos = lastpos;
        }
        return (
            <div>
                {this.loadingDiv()}
                <Map
                    pos={pos}
                    isMarkerShown={this.state.isMarkerShown}
                    onMarkerClick={this.handleMarkerClick}
                    onClick={this.addMarker.bind(this)}
                    play={this.props.play}
                    user={this.props.user}
                    markers={markers}
                    onIdle={this.finishLoading.bind(this)}
                />
                <div className="static-modal">
                    <Modal show={this.state.show} onHide={this.handleClose}>
                        <Modal.Header closeButton>
                            <Modal.Title>Marker</Modal.Title>
                        </Modal.Header>
                        <Modal.Body>
                            Label: <input type="text" value={this.state.currentLabel} onChange={e => this.setState({ currentLabel: e.target.value })} />
                        </Modal.Body>
                        <Modal.Footer>
                            <Button className='btn btn-danger' onClick={this.removeMarker}>Delete</Button>
                            <Button className='btn btn-primary' onClick={this.updateMarker}>Save</Button>
                            <Button onClick={this.handleClose}>Close</Button>
                        </Modal.Footer>
                    </Modal>

                </div>
            </div>
        )
    }
}

export default MapComponent;
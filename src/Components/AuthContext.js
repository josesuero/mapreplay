import React, { Component } from 'react';
export const Context = React.createContext();

/**
 * Context Api
 * Shares:
 * User: current user
 * cookie: current cookie
 * markers: current users markers
 * loading: if page data is still loading
 */
class Provider extends Component {
  
  render() {
    const {user, cookie, markers, loading} = this.props;

    return (
      <Context.Provider value={{user, cookie, markers, loading}}>
        {this.props.children}
      </Context.Provider>
    );
  }
}

export default Provider;
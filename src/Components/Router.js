import React from "react";
import { Switch, Route } from 'react-router-dom'

import Admin from '../Pages/Admin';
import Login from '../Components/Login';
import Play from '../Pages/Play';
import UserMap from '../Pages/UserMap';

/**
 * Main Router
 * / Main usermap on record mode
 * /login login page - not used
 * /play/:uid playback page for a uid (secured)
 * /admin Admin console (secured)
 * @param {*} props 
 */
const Router = (props) => {
  return (
    <main>
      <Switch>
        <Route exact path='/' component={UserMap} />
        <Route exact path='/login' component={Login} />
        <Route path='/play/:uid' component={Play} />
        <Route path='/admin' component={Admin} />
      </Switch>
    </main>
  )
}

export default Router

// Import FirebaseAuth and firebase.
import React from 'react';
import StyledFirebaseAuth from 'react-firebaseui/StyledFirebaseAuth';
import firebase from 'firebase';
import firebaseui from 'firebaseui';
import { AuthConfig } from '../config';

const CLIENT_ID = AuthConfig.CLIENT_ID; //'366128662598-i1nhk2po91qu9l41jspeomuio597gsra.apps.googleusercontent.com';


/**
 * SignIn screen, allows user to select an auth method, and login
 */
class SignInScreen extends React.Component {

    // The component's Local state.
    state = {
        isSignedIn: false // Local signed-in state.
    };

    /**
     * Callbacks after users has signed in
     * Signin options available: Google, Email, Facebook, Phone
     */
    uiConfig = {
        'callbacks': {
            // Called when the user has been successfully signed in.
            'signInSuccessWithAuthResult': function (authResult, redirectUrl) {
                if (authResult.user) {
                    this.handleSignedInUser(authResult.user);
                }
                if (authResult.additionalUserInfo) {
                    document.getElementById('is-new-user').textContent =
                        authResult.additionalUserInfo.isNewUser ?
                            'New User' : 'Existing User';
                }
                // Do not redirect.
                return false;
            }
        },
        // Opens IDP Providers sign-in flow in a popup.
        'signInFlow': 'popup',
        'signInOptions': [
            // TODO(developer): Remove the providers you don't need for your app.
            {
                provider: firebase.auth.GoogleAuthProvider.PROVIDER_ID,
                // Required to enable this provider in One-Tap Sign-up.
                authMethod: 'https://accounts.google.com',
                // Required to enable ID token credentials for this provider.
                clientId: CLIENT_ID
            },
            //   {
            //     provider: firebase.auth.FacebookAuthProvider.PROVIDER_ID,
            //     scopes :[
            //       'public_profile',
            //       'email',
            //       'user_likes',
            //       'user_friends'
            //     ]
            //   },
            //   firebase.auth.TwitterAuthProvider.PROVIDER_ID,
            //   firebase.auth.GithubAuthProvider.PROVIDER_ID,
            {
                provider: firebase.auth.EmailAuthProvider.PROVIDER_ID,
                // Whether the display name should be displayed in Sign Up page.
                requireDisplayName: true
            },
            //   {
            //     provider: firebase.auth.PhoneAuthProvider.PROVIDER_ID,
            //     recaptchaParameters: {
            //       size: this.getRecaptchaMode()
            //     }
            //   }
        ],
        // Terms of service url.
        'tosUrl': 'https://www.google.com',
        'credentialHelper': CLIENT_ID && CLIENT_ID !== 'YOUR_OAUTH_CLIENT_ID' ?
            firebaseui.auth.CredentialHelper.GOOGLE_YOLO :
            firebaseui.auth.CredentialHelper.ACCOUNT_CHOOSER_COM
    };

    // Listen to the Firebase Auth state and set the local state.
    componentDidMount() {
        this.unregisterAuthObserver = firebase.auth().onAuthStateChanged(
            (user) => this.setState({ isSignedIn: !!user })
        );
    }

    /**
     * Closes login modal
     */
    componentDidUpdate() {
        if (this.state.isSignedIn && this.props.visible) {
            this.props.onLogin();
        }
    }

    // Make sure we un-register Firebase observers when the component unmounts.
    componentWillUnmount() {
        this.unregisterAuthObserver();
    }

    getRecaptchaMode() {
        return 'invisible';
        // return location.hash.indexOf('recaptcha=invisible') !== -1 ?
        //     'invisible' : 'normal';
    }

    render() {
        if (!this.state.isSignedIn) {
            return (
                <div>
                    <p>Please sign-in:</p>
                    <StyledFirebaseAuth uiConfig={this.uiConfig} firebaseAuth={firebase.auth()} />
                </div>
            );
        }
        return (
            <div>
                <h1>My App</h1>
                <p>Welcome {firebase.auth().currentUser.displayName}! You are now signed-in!</p>
                <a onClick={() => firebase.auth().signOut()}>Sign-out</a>
            </div>
        );
    }
}


export default SignInScreen;
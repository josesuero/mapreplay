import React, { Component } from 'react';
import firebase from 'firebase';
import { BrowserRouter } from "react-router-dom";

import Cookies from 'universal-cookie';

import 'firebase/firestore';
import './App.css';

import AuthContext from './Components/AuthContext';
import DB from './Components/db';
import Header from './Components/Header';
import Router from './Components/Router';


import { firebaseConfig } from './config';

/**
 * React Application users context api to share user, cookie and markers
 * Generates a new uid for the cookie or loads a current one
 * Loads markers from firebase for the current user
 */
class App extends Component {
  constructor() {
    super();

    firebase.initializeApp(firebaseConfig);
    this.db = new DB();
    const cookies = new Cookies();

    const cookie = cookies.get('uid') || this.db.generatePushID();

    this.state = {
      user: null,
      markers: {},
      cookie: cookie,
      loading: false,
      first: cookies.get('uid') === undefined
    }

    cookies.set('uid', cookie, { path: '/' });
    const $this = this;

    firebase.auth().onAuthStateChanged((function (user) {
      if (!user) {
        $this.setState({ user });
        $this.db.updateUser({ cookie, lastUpdate: new Date() });
        $this.getMarkers.bind($this)(cookie);
        return;
      }

      $this.db.getUserByRegUid(user.uid).then(querySnapshot => {
        return new Promise((resolve, reject) => {
          if (querySnapshot.empty) {
            const {
              displayName,
              email,
              uid,
            } = user;
            resolve({
              cookie,
              lastUpdate: new Date(),
              displayName,
              email,
              uid,
            });
          } else {
            querySnapshot.forEach(function (doc) {

              //remove temporary session 
              const temp = cookies.get('uid');
              if (temp !== doc.id) {
                $this.db.removeUser(temp);
              }
              $this.setState({ cookie: doc.id });
              cookies.set('uid', doc.id, { path: '/' });

              $this.getMarkers.bind($this)(doc.id);
              resolve({ cookie: doc.id, lastUpdate: new Date(), ...doc.data() });
            });
          }
        });
      }).then(userObj => {
        return $this.db.updateUser(userObj).then(user => {
          return userObj;
        });
      }).then(user => {
        $this.setState({ user });
      });
    }));
  }

  /**
   * Loads all user markers from firebase to be filled on context api
   * @param {*} cookie Current uid
   */
  getMarkers(cookie) {
    this.db.getMarkers(cookie).then((querySnapshot) => {
      const markers = this.state.markers;
      querySnapshot.forEach((doc) => {
        markers[doc.id] = doc.data();
      });
      return markers;
    }).then(markers => {
      this.setState({
        markers,
      });
    });
  }

  render() {
    const { user, cookie, markers, loading } = this.state;
    return (
      <BrowserRouter>
        <AuthContext user={user} cookie={cookie} markers={markers} loading={loading}>
          <Header first={this.state.first}/>
          <Router />
        </AuthContext>
      </BrowserRouter>
    );
  }
}

export default App;

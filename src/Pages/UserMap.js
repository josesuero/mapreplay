import React, { Component } from 'react';

import { mapConfig } from '../config';

import { Context } from '../Components/AuthContext';
import MapComponent from '../Components/MapComponent';

/**
 * Loads the map component on record mode (play=false) with the users current markers and loading status loaded from context api
 */
class UserMap extends Component {

    render() {
        return (
            <div>
                <Context.Consumer>
                    {value => <MapComponent pos={mapConfig.initial} play={false} user={value.cookie} markers={value.markers} loading={value.loading} />}
                </Context.Consumer>
            </div>
        );
    }
}

export default UserMap;
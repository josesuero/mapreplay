import React, { Component } from 'react';
import Slider from 'react-rangeslider';

import 'react-rangeslider/umd/rangeslider.min.css';

import { mapConfig } from '../config';
import { Context } from '../Components/AuthContext';
import DB from '../Components/db';
import Loading from '../Components/Loading';
import MapComponent from '../Components/MapComponent';

/**
 * Playback of the recored positions
 * Loads uid from react-router parameter
 * Loads all positions available for the user, and saves them to global class prop events;
 * NOTE: if user doesn't have permission for the playback it will remain on loading mode forever
 */
class Play extends Component {
    constructor(props, context) {
        super(props, context);
        const user = this.props.match.params.uid;
        this.state = {
            value: 0,
            loading: true,
            user
        }
        this.db = new DB();
        this.events = [];
    }

    /**
     * Loads positions from DB and adds them to events, then loads user properties and sets state with user, and initial position
     */
    componentWillMount() {
        const $this = this;
        this.db.get(this.state.user).then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                //console.log(`${doc.id} => ${doc.data()}`);
                $this.events.push(doc.data());
            });
        }).then(() => {
            return $this.db.getUser($this.state.user);
        }).then(user => {
            // console.log('user', user.data());
            //user.forEach(doc=> console.log(doc.data()));
            $this.setState({
                pos: $this.events[0],
                loading: false,
                user: user.data()
            });
        });
    }

    /**
     * Capitalize first letter of the string received
     * @param {*} string 
     */
    capitalizeFirst(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

    /**
     * Event when slider starts changing positions
     */
    handleChangeStart = () => {
        console.log('Change event started')
    };

    /**
     * Event when slider has finish changing, sets current position to the value of the slider
     */
    handleChange = value => {
        //console.log(this.events[value]);
        this.setState({
            value: value,
            pos: this.events[value]
        });
    };

    /**
     * Event when slider has ended changing positions
     */
    handleChangeComplete = () => {
        console.log('Change event completed')
    };

    render() {
        let { uid, value, pos, user } = this.state;
        pos = pos || mapConfig.initial;
        
        return (
            <Context.Consumer>
                {contextValue => {
                    if (!contextValue.user || contextValue.user.isAdmin !== true) {
                        return <Loading />
                    }
                    return (
                        <div className='slider'>
                            <MapComponent pos={pos} play={true} user={uid} markers={pos.markers} loading={this.state.loading} />
                            <Slider
                                min={0}
                                max={this.events.length - 1}
                                value={value}
                                onChangeStart={this.handleChangeStart}
                                onChange={this.handleChange}
                                onChangeComplete={this.handleChangeComplete}
                            />
                            <div className='value'>{pos.date ? pos.date.toString() : ''}</div>
                            <div>
                                <table className="table table-striped table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Field</th>
                                            <th>Value</th>
                                        </tr>
                                    </thead>


                                    <tbody>
                                        {Object.keys(user).map(key => <tr key={key}><td>{this.capitalizeFirst(key)}</td><td> {user[key].toString()} </td></tr>)}
                                    </tbody>
                                </table>



                            </div>
                        </div>);
                }}
            </Context.Consumer>
        );


    }
}

export default Play;
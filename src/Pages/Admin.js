import React, { Component } from 'react';
import moment from 'moment';
import { Link } from "react-router-dom";

import ReactTable from "react-table";
import "react-table/react-table.css";

import { Context } from '../Components/AuthContext';
import DB from '../Components/db';
import Loading from '../Components/Loading';

/**
 * Admin interface
 * Lists all users on DB
 * allows to view users on play back or remove all users positions
 * NOTE: if user doesn't have permission for the admin it will remain on loading mode forever
 */
class Admin extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            value: 0,
            loading: true,
            users: []
        }
        this.db = new DB();
    }

    /**
     * Loads all users from DB, formats date and adds them to state
     */
    componentWillMount() {
        const users = [];
        this.db.getUsers().then((querySnapshot) => {
            querySnapshot.forEach((doc) => {
                const data = doc.data();
                data.lastUpdate = moment(data.lastUpdate).format("YYYY-MM-DD hh:mm:ss A");
                users.push({ uid: doc.id, ...data });
            });
        }).then(() => {
            this.setState({
                loading: false,
                users
            });
        });
    }

    /**
     * removed all positions for the user
     */
    removePositions(e) {
        e.preventDefault();
        this.db.removePositions(e.target.id).then(() => {
            alert('Positions deleted');
        });
    }

    render() {
        const data = this.state.users;
        return (<Context.Consumer>
            {value => {
                if (!value.user || value.user.isAdmin !== true) {
                    return <Loading />
                }
                return (<div>
                    <ReactTable
                        data={data}
                        columns={[
                            {
                                Header: "Id",
                                id: "cookie",
                                accessor: "cookie"
                            },
                            {
                                Header: "Name",
                                id: "name",
                                accessor: "displayName"
                            },
                            {
                                Header: "Email",
                                id: "email",
                                accessor: "email"
                            },
                            {
                                Header: "Region",
                                id: "region",
                                accessor: "region"
                            },
                            {
                                Header: "Created Date",
                                accessor: "lastUpdate"
                            },
                            {
                                Header: "Last Session",
                                accessor: "lastUpdate"
                            },
                            {
                                Header: "View",
                                id: "lastName",
                                //accessor: d => d.lastName,
                                Cell: ({ row }) => <Link to={`/play/${row.cookie}`}>View</Link>
                            },
                            {
                                Header: "Remove",
                                id: "lastName",
                                //accessor: d => d.lastName,
                                Cell: ({ row }) => (
                                    <a href="#remove" id={row.uid} onClick={this.removePositions.bind(this)}>Remove</a>
                                )
                            },

                        ]}
                        defaultPageSize={10}
                        className="-striped -highlight"
                    />
                    <br />
                </div>)
            }}
        </Context.Consumer>)
    }
}

export default Admin;
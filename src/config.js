/**
 * Firebase project Configuration
 */
export const firebaseConfig = 
{
    "apiKey": "AIzaSyCn0tvn15Liff_I8X7NV0B51BuI2S3DvVQ",
    "authDomain": "mapreplay-a1313.firebaseapp.com",
    "databaseURL": "https://mapreplay-a1313.firebaseio.com",
    "projectId": "mapreplay-a1313",
    "storageBucket": "mapreplay-a1313.appspot.com",
    "messagingSenderId": "366128662598"
};
/**
 * Map configuration
 * kmz: url of kmz or kml
 * initial: initial position and zoom of the map
 * Key: google maps key
 * labelColor: Marker label color
 * labelSize: Marker label size
 */
export const mapConfig = {
    //kmz: "https://mapreplay-a1313.firebaseapp.com/example.dmz",
    kmz: "https://s3.amazonaws.com/mapreplay.mstn.com/example.kmz",
    initial: {lat: 42.342286, lng: -110.242192, zoom: 6},
    key: "AIzaSyDGP_LzJuU0A_Qk5EGDEeg1QAke1s09Xdo",
    labelColor: 'black',
    labelSize: '14px',
}
/**
 * Auth config
 * CLIENT_ID: Google client id for login with google accounts
 */
export const AuthConfig = {
    CLIENT_ID: '366128662598-i1nhk2po91qu9l41jspeomuio597gsra.apps.googleusercontent.com'
}